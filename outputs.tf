output "api_urls" {
  value = "${module.example_deploy_prod.api_urls}"
}

output "api_list" {
  value = "${module.example_deploy_prod.description}"
}

output "api_raw_urls" {
  value = "${module.example_deploy_prod.raw_urls}"
}
