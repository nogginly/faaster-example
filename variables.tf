variable "aws_region" {
  default = "us-east-1"
}

variable "deploy_name" {
  default = "faaster-example"
}

variable "base_domain_name" {
  default = ""
}
