# FaaSter Example Changelog

The latest version of this file can be found at the master branch of the [nogginly/faaster-example](https://gitlab.com/nogginly/faaster-example) repository.

## 0.4.0

* Updated with `faaster` version 0.12.0, fixing module references.
* Complies with `terraform` 0.12.x as well as 0.11.x.

## 0.3.0

* Updated with `faaster` version 0.10.0, fixing `api_deploy` usage. Not using the multi-stage method modules yet.

## 0.2.0

* Cleaning up README. 
* Fixing VERSION file.

## 0.1.0

* Split out the example formerly in [nogginly/faaster](https://gitlab.com/nogginly/faaster) into its own repository here.

