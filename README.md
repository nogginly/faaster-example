# FaaSter Example

This is an example REST API defined and deployed using [Faaster](https://gitlab.com/nogginly/faaster), a set of `terraform` modules that make it faster to deploy "serverless" microservices using Amazon's Lambda, API Gateway, Dynamo DB and S3 services (AWS).

## Compatibility

This examples is compatible with `terraform` versions 0.11.x _and 0.12.x_.

## Overview

The diagram below shows an overview based on the [Faaster](https://gitlab.com/nogginly/faaster) modules and how they're wired up.

![](diagrams/faaster-example-overview.png)

## Getting Started

### Pre-conditions

1. You need to have an AWS account set up.
2. You need an API key set up, either as a credential for your root account or by setting up an IAM user with admin access.
3. You need the most recent version of [`terraform`](https://terraform.io) installed on your system. 
    - I use `brew` (or `linuxbrew`) to install it and keep it up to date.
4. Clone this repo.

### Getting Ready

1.Use your terminal/console for the following steps.

```bash
$ cd faaster-example/
$ mkdir -p deployments/mydeployment
```

2. Go into the `mydeployment/` folder
4. Create a file called `credentials` and make sure it exports the following env variables:
    - `export AWS_ACCESS_KEY_ID=YOUR_ACCOUNT_ADMIN_ACCESS_KEY`
    - `export AWS_SECRET_ACCESS_KEY=YOUR_ACCOUNT_ADMIN_SECRET_KEY`
5. Create a file called `terraform.tfvars` and make sure it has the following properties:
    - `aws_region = YOUR_AWS_REGION_IF_NOT_US_EAST_1`
    - `deploy_name = "example"`
7. Now use `cd` to back out so that you are back in the `faaster-example/` folder.

### Initialize

Run the following command to initialize your local account folder using the configuration.

```bash
$ ./bin/faasterraform mydeployment init
```

You should get a success message like this:

```
Terraform has been successfully initialized!
```

### Check the plan

Run the following command to plan the configuration of your AWS account. 

```bash
$ ./bin/faasterraform mydeployment plan
```

A lot of stuff will scroll by but eventually you should at least see a line like the following:

```
Plan: 17 to add, 0 to change, 0 to destroy.
```

### Apply the plan

Run the following command to apply the configuration to your AWS account. 

```bash
$ ./bin/faasterraform mydeployment apply
```

You will be prompted to confirm by typing in `yes` and then you will need to wait as all the resources get provisioned. You will get the following output:

```
Apply complete! Resources: 17 added, 0 changed, 0 destroyed.

Outputs:

api = {
  LIST = POST/message, GET/message/{id}
  RAWURL = https://API_ID.execute-api.us-east-1.amazonaws.com/api
  URL = https://API_ID.execute-api.us-east-1.amazonaws.com/api
}
```

Using your favorite tool to make HTTP requests, try out the following requests, replacing `API_ID` in the URLs with the one above from your `terraform` output.

1. GET: `https://API_ID.execute-api.us-east-1.amazonaws.com/api/message/xyz1237`
2. POST: `https://API_ID.execute-api.us-east-1.amazonaws.com/api/message` with following JSON body:
  * `{ "key1": "Roly", "key2" : "Poly", "key3": "What a moly!" }`

### Cleanup (Undo) the plan

You can cleanup the example with the following command.

```bash
$ ./bin/faasterraform.sh mydeployment destroy
```

