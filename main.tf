provider "aws" {
  region = "${var.aws_region}"
}

provider "archive" {}

#
# Deploys a `/message` resource via API gateway with
#   - GET handler
#   - POST handler
#
module "example_api" {
  source        = "./modules/faaster/aws/api"
  api_name      = "Faaster Example API"
  api_desc      = "Example API to show how to use `faaster` modules to get started"
  deploy_prefix = "${var.deploy_name}"
}

module "example_message_base" {
  source    = "./modules/faaster/aws/api_resource"
  path_name = "message"
  api_id    = "${module.example_api.id}"
  parent_id = "${module.example_api.root_id}"
}

module "example_message_by_id" {
  source = "./modules/faaster/aws/api_resource"

  # The use of `{xxx}` resource allows us to handle a parameter in the request "route"
  # E.g. /message/{id}
  path_name = "{id}"

  api_id    = "${module.example_api.id}"
  parent_id = "${module.example_message_base.id}"
}

module "ex_message_POST" {
  source        = "./modules/faaster/aws/api_lambda_method"
  fn_name       = "message_POST"
  http_method   = "POST"
  api_id        = "${module.example_api.id}"
  resource_id   = "${module.example_message_base.id}"
  resource_path = "${module.example_message_base.path}"
  opt_cwlogging = "yes"
  deploy_prefix = "${var.deploy_name}"
}

module "ex_message_GET" {
  source        = "./modules/faaster/aws/api_lambda_method"
  fn_name       = "message_GET"
  http_method   = "GET"
  api_id        = "${module.example_api.id}"
  resource_id   = "${module.example_message_by_id.id}"
  resource_path = "${module.example_message_by_id.path}"
  opt_cwlogging = "yes"
  deploy_prefix = "${var.deploy_name}"
}

module "example_deploy_prod" {
  source = "./modules/faaster/aws/api_deploy"

  stages = {
    "api" = {}
  }

  api_id        = "${module.example_api.id}"
  deploy_prefix = "${var.deploy_name}"
  domain_name   = "${var.base_domain_name}"
  domain_prefix = "${var.base_domain_name == "" ? "" : var.deploy_name}"

  #
  # This is used to explicitly specify a dependency to the methods because
  # otherwise the deployment gets created prematurely and fails.
  method_types = [
    "${module.ex_message_POST.type}",
    "${module.ex_message_GET.type}",
  ]
}
